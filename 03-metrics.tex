\subsection{Measuring Distortion} \label{sec:metrics}
A number of metrics already exist for quantifying the angular and area distortion introduced by parameterization. We use these metrics in conjunction with the generated texture images to guide our evaluation of the visual artifacts introduced by flattening distortion.

It is important to note that previous work has largely evaluated the effect of parameterization distortion as it pertains to mapping a 2D texture onto a 3D object. Parameterizations that are not isometric or conformal lead to texture information that is ``correct'' in 2D but which becomes distorted when mapped to 3D. In our case, texture information is derived from the 3D volume and then mapped to 2D space. Thus, parameterization error leads to 2D artifacts in texture information rather than 3D ones.

%%% L^2(T) - L^2 stretch
%%% L^inf(T) - Largest Local Stretch
Sander \textit{et al.} define the $L^2$ and $L^{\infty}$ per-triangle texture stretch metrics \cite{sander2001texture}. These metrics describe the average and worst case stretch scenarios across the set of faces, respectively, and are an indicator of isometric error. $L^2$ is the root-mean-square stretch across the face of the considered triangle, while $L^{\infty}$ represents the largest local stretch---the greatest length obtained when mapping a unit vector from the parameterized triangle space to the 3D surface. We measure the corresponding per-mesh metrics as described by Sander \textit{et al.} and present the results in Tables \ref{tab:l2error} and \ref{tab:linferror}.

%%% Angular distortion
% Per-face
% \sum_{j=1}^{3} (\alpha_i - \phi_i)^2w_i
% \phi_i = \beta_i * 2\pi / \sum original angles incident to same vertex
% w_i = (\phi_i)^{-2} // Relative angular distortion error
ABF introduces its own angular distortion metric for minimizing conformal error:
\begin{equation}
F(\alpha) = \sum_{i=1}^{T}\sum_{j=1}^{3} w_j^i(\alpha_j^i - \phi_j^i)^2
\end{equation}
where $i$ iterates through the set of face triangles $T$, $j$ iterates through the angles of each face, $w_j^i$ is a per-angle weight, and $\alpha_j^i$ and $\phi_j^i$ are the parameterized and optimal angles respectively. This is the same metric used as part of the minimization task and can be evaluated for the whole mesh by averaging the error across the total number of angles:

% Per mesh
% Sum of above calculation for each face / 3n
% n = number of faces
\begin{equation}
F(M)=\frac{F(\alpha)}{3T}
\end{equation}

%%% Blender - Area
% fa = face_area/total_face_area
% uva = uv_area/total_uv_area
% if fa > uva:
%    1.0 - (uva/fa)
% else
%    1.0 - (fa/uva)
Blender, the open-source 3D modeling and rendering application, utilizes both angular and area error metrics for displaying parameterization error to the user \cite{blender2016}. We make use of the per-triangle area metric:
\begin{equation}
\begin{gathered}
\alpha = \frac{A(T)}{A(M)},\ \beta = \frac{A(t)}{A(m)}\\
E(t) = 
\begin{cases}
      1.0-\frac{\beta}{\alpha},& \text{if } \alpha>\beta\\
      1.0-\frac{\alpha}{\beta},& \text{otherwise}
\end{cases}
\end{gathered}
\end{equation}
where $A(x)$ is the area function, $T$ and $M$ represent a 3D triangle and mesh, and $t$ and $m$ represent the corresponding 2D triangle and mesh. We calculate a per-mesh value $E(M)$ by taking the mean of the per-triangle error. We also made use of Blender's parameterization error visualization to produce the error distribution images shown in Appendix \ref{sec:figures}.